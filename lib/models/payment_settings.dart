import 'package:flutter/material.dart';

import '../services/index.dart';
import 'credit_card.dart';

class PaymentSettingsModel extends ChangeNotifier {
  final Services _service = Services();
  PaymentSettings paymentSettings;
  bool isLoading = true;
  String message;
  String cardVaultUrl;

  Future<void> getPaymentSettings() async {
    try {
      paymentSettings = await _service.getPaymentSettings();

      isLoading = false;
      message = null;
      notifyListeners();
    } catch (err) {
      isLoading = false;
      message =
          "There is an issue with the app during request the data, please contact admin for fixing the issues " +
              err.toString();
      notifyListeners();
    }
  }

  Future<String> getVaultId(PaymentSettingsModel paymentSettingsModel,
      CreditCardModel creditCardModel) async {
    try {
      paymentSettings =
          await _service.addCreditCard(paymentSettingsModel, creditCardModel);

      isLoading = false;
      message = null;
      notifyListeners();
    } catch (err) {
      isLoading = false;
      message =
          "There is an issue with the app during request the data, please contact admin for fixing the issues " +
              err.toString();
      notifyListeners();
      return null;
    }
    return paymentSettings.vaultId;
  }

  String getCardVaultUrl() {
    return paymentSettings.cardVaultUrl;
  }
}

class PaymentSettings {
  /// https://shopify.dev/tutorials/create-a-checkout-with-storefront-api
  String vaultId; // apply when checkout credit card

  String cardVaultUrl;
  List<String> acceptedCardBrands;
  String countryCode;
  String currencyCode;

  PaymentSettings(
      {this.cardVaultUrl,
      this.acceptedCardBrands,
      this.countryCode,
      this.currencyCode});

  Map<String, dynamic> toJson() {
    return {
      "cardVaultUrl": cardVaultUrl,
      "acceptedCardBrands": acceptedCardBrands,
      "countryCode": countryCode,
      "enabled": currencyCode
    };
  }

  PaymentSettings.fromShopifyJson(Map<String, dynamic> parsedJson) {
    cardVaultUrl = parsedJson["cardVaultUrl"];
    acceptedCardBrands = parsedJson["acceptedCardBrands"];
    countryCode = parsedJson["countryCode"];
    currencyCode = parsedJson["currencyCode"];
  }

  PaymentSettings.fromVaultIdShopifyJson(Map<String, dynamic> parsedJson) {
    vaultId = parsedJson["vaultId"];
  }
}
