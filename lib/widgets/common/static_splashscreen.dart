import 'package:flutter/material.dart';
import 'package:after_layout/after_layout.dart';

import '../../common/tools.dart';

class StaticSplashScreen extends StatefulWidget {
  final String imagePath;
  final Widget onNextScreen;
  final int duration;
  final Key key;
  StaticSplashScreen({this.imagePath, this.key, this.onNextScreen, this.duration = 2500});

  @override
  _StaticSplashScreenState createState() => _StaticSplashScreenState();
}

class _StaticSplashScreenState extends State<StaticSplashScreen> with AfterLayoutMixin{
  @override
  void afterFirstLayout(BuildContext context) {
    Future.delayed(Duration(milliseconds: widget.duration), () {
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => widget.onNextScreen));
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: widget.imagePath.startsWith('http')
            ? Tools.image(
                url: widget.imagePath,
                fit: BoxFit.cover,
              )
            : Image.asset(widget.imagePath,
                gaplessPlayback: true, fit: BoxFit.cover),
      ),
    );
  }
}
