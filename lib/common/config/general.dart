import '../../common/constants.dart';

/// Default app config, it's possible to set as URL
const kAppConfig = 'lib/config/config_en.json';

/// This option is determine hide some components for web
var kLayoutWeb = identical(0, 0.0);

/// The Google API Key to support Pick up the Address automatically
/// We recommend to generate both ios and android to restrict by bundle app id
/// The download package is remove these keys, please use your own key
const kGoogleAPIKey = {
  "android": "AIzaSyAfsfWe_uQVBOhWe00wUAqFRWq-iXtH3Y0",
  "ios": "your-google-api-key",
  "web": "your-google-api-key"
};

/// user for upgrader version of app, remove the comment from lib/app.dart to enable this feature
/// https://tppr.me/5PLpD
const kUpgradeURLConfig = {
  "android":
      "https://play.google.com/store/apps/details?id=com.inspireui.fluxstore",
  "ios": "https://apps.apple.com/us/app/mstore-flutter/id1469772800"
};

/// use for rating app on store feature
const kStoreIdentifier = {
  "android": "", // ex: com.inspireui.fluxstore
  "ios": "" // ex: 1469772800
};

const kAdvanceConfig = {
  "DefaultLanguage": "en",
  "DetailedBlogLayout": kBlogLayout.halfSizeImageType,
  "EnablePointReward": true,
  "hideOutOfStock": false,
  "EnableRating": true,
  "hideEmptyProductListRating": false,
  /// Show stock Status on product List
  "showStockStatus": true,

  "isCaching": false,

  /// set kIsResizeImage to true if you have finish running Re-generate image plugin
  "kIsResizeImage": false,

  "GridCount": 3,
  "DefaultCurrency": {
    "symbol": "RS",
    "decimalDigits": 0,
    "symbolBeforeTheNumber": false,
    "currency": "PKR"
  },
  "Currencies": [
    {
      "symbol": "RS",
      "decimalDigits": 0,
      "symbolBeforeTheNumber": false,
      "currency": "PKR"
    },
    {
      "symbol": "đ",
      "decimalDigits": 2,
      "symbolBeforeTheNumber": false,
      "currency": "VND"
    },
    {
      "symbol": "€",
      "decimalDigits": 2,
      "symbolBeforeTheNumber": true,
      "currency": "Euro"
    },
    {
      "symbol": "£",
      "decimalDigits": 2,
      "symbolBeforeTheNumber": true,
      "currency": "Pound sterling"
    },
  ],

  /// Below config is used for Magento store
  "DefaultStoreViewCode": "",
  "EnableAttributesConfigurableProduct": ["color", "size", "litre"],
  "EnableAttributesLabelConfigurableProduct": ["color", "size" , "litre"],

  //if the woo commerce website supports multi languages. set false if the website only have one language
  "isMultiLanguages": false
};

const kLoginSetting = {
  "IsRequiredLogin": false,
  'showAppleLogin': false,
  'showFacebook': false,
  'showSMSLogin': false,
  'showGoogleLogin': false,
};

const kDefaultDrawer = {
  "logo": null,
  "background":
      "https://www.soudaghar.com/pub/media/logo/stores/1/sauda-ghar-logo.png",
  "items": [
    {"type": "home", "show": true},
    {"type": "blog", "show": false},
    {"type": "login", "show": true},
    {"type": "category", "show": true}
  ]
};

const kDefaultSettings = [
  'wishlist',
  'notifications',
  // 'language',
  // 'currencies',
  'darkTheme',
  'order',
  // 'point',
  // 'rating',
  // 'privacy',
  'about'
];

/// Add your own onesignal App ID, example: 8b45b6db-7421-45e1-85aa-75e597f62xxx
const kOneSignalKey = {'appID': ''};
