import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../custom/smartchat.dart';
import '../../services/index.dart';
import '../../models/app.dart';

class CartScreen extends StatefulWidget {
  final bool isModal;
  final bool isBuyNow;
  final bool showChat;

  CartScreen({this.isModal, this.isBuyNow = false, this.showChat});

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  @override
  Widget build(BuildContext context) {
    bool showChat = widget.showChat ?? false;

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      floatingActionButton: showChat
          ? SmartChat(
              margin: EdgeInsets.only(
                right:
                    Provider.of<AppModel>(context, listen: false).locale == 'ar'
                        ? 30.0
                        : 0.0,
              ),
            )
          : Container(),
      body: SafeArea(
        top: true,
        child: Services().widget.renderCartPageView(
            isModal: widget.isModal,
            isBuyNow: widget.isBuyNow,
            pageController: pageController,
            context: context),
      ),
    );
  }
}
